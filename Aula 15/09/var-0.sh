#!/bin/bash

echo '$USER' - \n\nRetorna o Nome do Usuário.\n\n
echo '$UID' - Retorna o ID do Usuário.
echo '$PWD' - Retorna o Diretório Atual.
echo '$HOME' - Retorna o Diretório HOME do Usuário Atual.
echo '$PATH' - Obtém o PATH'(caminho)' Atual do Sistema.
echo '$LINES' - Retorna o Número de Linhas do Terminal.
echo '$COLUMNS' - Retorna o Número de Colunas do Terminal.
echo '$0-n' parametro de posições
echo '$?' Devolve o status de saída ou erro
echo '$BASH_VERSION' - Armazena a versão do shell bash em execução, em formato legível por humanos.
echo '$CDPATH' - Caminho de busca do comando cd.
echo '$COLUMNS' - Retorna o número de colunas do terminal.
echo '$DIRSTACK' - Pilha de diretórios disponíveis para os comandos pushd e popd.
echo '$DISPLAY' - Configura o valor do display X.
echo '$EDITOR' - Configurar o editor de texto padrão do sistema.
echo '$EUID' - Retorna o ID do usuário.
echo '$GROUPS' - Retorna informações sobre o GID.
echo '$HISTFILE' - Nome do arquivo no qual o histórico de comandos é salvo.
echo '$HISTFILESIZE' - Número máximo de linhas contidas no arquivo de histórico.
echo '$HOSTNAME' - Nome do computador
